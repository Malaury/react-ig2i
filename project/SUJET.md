<img src="header-projet-ig2i-2020.jpg">

## Sommaire <!-- omit in toc -->
- [A. Objectif](#a-objectif)
- [B. Arborescence](#b-arborescence)
- [C. Fonctionnalités](#c-fonctionnalités)
	- [Page d'Accueil](#page-daccueil)
	- [Page L'équipe](#page-léquipe)
	- [Page Choix technologiques](#page-choix-technologiques)
	- [Page Résultats de recherche](#page-résultats-de-recherche)
	- [Page détail de série](#page-détail-de-série)
- [D. Précisions techniques](#d-précisions-techniques)
	- [Accueil : Diaporama](#accueil-diaporama)
	- [Recherche](#recherche)
	- [navigation](#navigation)
	- [UX](#ux)
	- [UI](#ui)
- [E. Critères d'évaluation](#e-critères-dévaluation)
- [F. Modalités de rendu et deadline](#f-modalités-de-rendu-et-deadline)
- [G. Questions](#g-questions)

## A. Objectif
Au cours de ce projet vous aurez à réaliser une Single Page App qui doit permettre à un utilisateur de trouver la prochaine série qu'il spoilera dans ses prochains cours.

## B. Arborescence
L'application que vous développerez suivra l'arborescence suivante :
```
Accueil (url "/")
    ├─ L'équipe (url "/lequipe.fr")
    ├─ Choix technologiques (url "/choix-technologiques")
    ├─ Résultats de recherche (url "/resultats/:marecherche")
	└─ Détail série (url "/series/:nomdelaserie-:id")
```

## C. Fonctionnalités
### Page d'Accueil
Sur la page d'accueil l'utilisateur de votre site doit trouver :
- un diaporama de photos :
	- une seule photo affichée à la fois
	- le diaporama change automatiquement de photo toutes les 3 secondes
	- des boutons "précédent" / "suivant" permettent de passer à la photo... précédente / suivante !
	- lorsqu'on arrive à la dernière photo, l'appui sur le bouton "suivant" doit reboucler sur la première photo. A l'inverse, si l'on est sur la première, un appui sur le bouton "précédent" amène à la dernière
	- le diaporama doit fonctionner quelque soit le nombre de photos qu'on y place
- un formulaire, contenant un champ de saisie unique, qui permet à l'utilisateur de lancer une recherche (cf. page Résultats de recherche)

### Page L'équipe
Page statique dans laquelle vous présentez les membres de votre équipe. Pour chaque membre, vous devez indiquer :
- prénom
- nom
- surnom
- série préférée
- pourcentage de la note du groupe qui lui sera attribué

### Page Choix technologiques
Page statique dans laquelle vous expliquez les choix technologiques que vous avez fait (découpage du store, organisation des fichiers, librairies tierces employées, etc.).


### Page Résultats de recherche
Dans cette page on présente à l'utilisateur la liste des séries qui correspondent à la chaîne de caractères qu'il a saisi dans le formulaire de recherche.

Cette page contient :
- un formulaire de recherche (le même que sur la page d'accueil) pré-rempli avec la chaîne de caractères recherchée
- un titre avec la chaîne recherchée et le nombre de résultats
- des boutons qui permettent de changer l'ordre de tri des résultats :
	- par score de pertinence décroissant (par défaut)
	- par note décroissante
	- par date de première diffusion
- la liste des résultats avec :
	- l'image d'illustration de la série
	- le titre de la série
	- sa date de première diffusion
	- son résumé
	- sa note
- des liens de pagination (facultatif, mais des points bonus sont rajoutés pour les équipes qui le feront) qui permettent d'afficher les résultats par groupes de 5

### Page détail de série
Dans cette page vous afficherez les information détaillées de la série :
en plus des informations de la page de résultats vous afficherez :
- un lien vers le site officiel de la série
- la liste des 5 derniers épisodes de la série :
	- titre
	- date de diffusion
	- image si elle existe
	- résumé de l'épisode (masqué par défaut mais un bouton "spoil" permet de l'afficher/masquer)


## D. Précisions techniques
### Accueil : Diaporama
Si de nombreuses librairies React existent pour générer des diaporamas photos, vous avez interdiction d'en utiliser une toute faite, sinon ce n'est pas drôle.

Pour le défilement automatique, vous pouvez utiliser la fonction [setInterval](https://developer.mozilla.org/fr/docs/Web/API/WindowOrWorkerGlobalScope/setInterval). Pensez à appeler [clearInterval](https://developer.mozilla.org/fr/docs/Web/API/WindowTimers/clearInterval) lors du [`componentWillUnmount()`](https://reactjs.org/docs/react-component.html#componentwillunmount) de votre composant pour éviter des fuites mémoire.



### Recherche
L'api que je vous propose d'utiliser pour la recherche de séries est l'api de TVMaze : https://www.tvmaze.com/api

Elle a l'avantage d'être gratuite, rapide et sans besoin de clé d'api ou de token d'authentification.

### navigation
Toutes les actions de l'utilisateur (sauf le défilement dans le diaporama) doivent avoir un impact sur l'url du site.

Par exemple, si j'ai recherché la chaîne de caractères "breaking" dans le moteur de recherche, que j'ai choisi un tri par "note", et que je suis allé sur la 2e page de résultats, si je rafraîchis mon navigateur, je dois retomber exactement là où j'étais.

### UX
Chaque chargement de données doit être signalé à l'utilisateur (par le biais d'un loader, ou d'un message de chargement)

### UI
Vous avez le choix d'utiliser un framework CSS pour l'apparence de votre application ou de partir de zéro (hashtag #warrior).

Même si le but est d'évaluer vos compétences en développement React, nous savons tous qu'une application, même la meilleure, si elle n'a pas une interface agréable à utiliser ne sera pas utilisée. Ici votre public, c'est moi, et je ne le suis pas bon, public, justement. Je porterai donc une attention particulière à la qualité de mise en page de votre application.

Pour info, voilà à quoi ressemble mon prototype (incomplet) du projet :
<img src="./reactflix.jpg">

## E. Critères d'évaluation
Vous serez évalués sur :
- le respect du cahier des charges
- la qualité du code de votre application ([DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas), [YAGNI](https://fr.wikipedia.org/wiki/YAGNI), [KISS](https://fr.wikipedia.org/wiki/Principe_KISS))
- les performances
- l'absence de similitudes avec le code des autres équipes ou du code trouvé en ligne (je connais les Internets)
- la propreté du design de votre application et son ergonomie
- la tête du client

## F. Modalités de rendu et deadline
J'attends vos projets via un dépot git privé (framagit.org ou gitlab.com par exemple) pour le vendredi 19 à 23h59 au plus tard. Seuls les membres de votre équipe doivent avoir accès à ce repo (en plus de moi).

## G. Questions
En cas de questions, n'hésitez pas à m'en faire part dans le channel #projet du serveur discord.

<img src="https://media1.tenor.com/images/6bbb6d09cf496dd3a06ff0e062b6298f/tenor.gif?itemid=14699764">
