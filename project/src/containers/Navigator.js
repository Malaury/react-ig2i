import React, { useState } from 'react';
import Home from './Home';
import TechChoice from './TechChoice';
import ResearchResult from './ResearchResult';
import SerieDetail from './SerieDetail';
import Team from './Team';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
} from 'reactstrap';

import { connect } from 'react-redux';
import SearchInput from '../components/SearchInput';

const mapStateToProps = ({ search }) => ({
	search: search,
});

function Navigator(props) {
	const { search } = props;

	const [collapsed, setCollapsed] = useState(false);

	const location =
		search.query !== '' ? (
			<Redirect to={`/resultats/${search.query}`} />
		) : (
			<Redirect to="/" />
		);

	return (
		<Router>
			{location}
			<Navbar className="navbar navbar-expand-lg navbar-dark bg-dark">
				<NavbarBrand className="navbar-brand">Reactiflix</NavbarBrand>
				<NavbarToggler
					onClick={() => {
						setCollapsed(!collapsed);
					}}
					className="mr-2"
				/>

				<Collapse isOpen={collapsed} style={{ position: 'relative' }} navbar>
					<Nav navbar className="navbar-nav mr-auto">
						<NavItem className="nav-item">
							<Link className="nav-link" to="/">
								Accueil
							</Link>
						</NavItem>
						<NavItem className="nav-item">
							<Link className="nav-link" to="/lequipe.fr">
								L'équipe
							</Link>
						</NavItem>
						<NavItem className="nav-item">
							<Link className="nav-link" to="/choix-technologiques">
								Technologies
							</Link>
						</NavItem>
						<div
							className="form-inline my-2 my-lg-0"
							style={collapsed ? {} : { position: 'absolute', right: 0 }}
						>
							<SearchInput />
						</div>
					</Nav>
				</Collapse>
			</Navbar>
			<Route exact path="/" component={Home} />
			<Route exact path="/lequipe.fr" component={Team} />
			<Route exact path="/choix-technologiques" component={TechChoice} />
			<Route exact path="/resultats/:marecherche" component={ResearchResult} />
			<Route exact path="/series/:nomdelaserie-:id" component={SerieDetail} />
		</Router>
	);
}

export default connect(mapStateToProps)(Navigator);
