import React, { Component } from 'react';
import Tech from '../components/Tech';
import techData from '../data/technos.json';

export default class TechChoice extends Component {
	state = {
		techs: [],
	};

	componentDidMount() {
		this.setState({ techs: techData });
	}

	render() {
		return (
			<div className="jumbotron">
				<h1 className="display-3">Les choix technologiques</h1>
				<div className="my-4">{this.renderTech()}</div>
			</div>
		);
	}

	renderTech() {
		return this.state.techs.map(({ id, title, description }) => (
			<Tech key={id} title={title} description={description}></Tech>
		));
	}
}
