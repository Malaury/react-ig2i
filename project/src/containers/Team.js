import React, { useState } from 'react';
import Profile from '../components/Profile';
import profileData from '../data/profileData';

const Team = () => {
	const [profiles] = useState(profileData);

	return (
		<div className="jumbotron">
			<h1 className="display-3">L'équipe !</h1>
			<div className="my-4">
				<div className="row" style={{ margin: 10 }}>
					{profiles.map(profile => {
						return (
							<Profile
								key={profile.id}
								firstname={profile.firstname}
								fullname={profile.fullname}
								nickname={profile.nickname}
								img={profile.img}
								title={profile.title}
								favoriteShow={profile.favoriteShow}
								evaluationPercentage={profile.evaluationPercentage}
							></Profile>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default Team;
