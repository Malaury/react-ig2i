import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import SerieThumbnail from '../components/SerieThumbnail';

const mapStateToProps = ({ search }) => ({
	search,
});

const ResearchResult = ({ search }) => {
	const { query, results } = search;

	const [series, setSeries] = useState(results);
	const [filtered, setFiltered] = useState({
		note: false,
		date: false,
	});

	function changeSeries() {
		if (!results) return;
		let seriesTemp = results;
		//console.log(filtered);
		if (filtered.note) {
			//console.log(seriesTemp);
			seriesTemp = seriesTemp
				.filter(serie => serie.show.rating && serie.show.rating.average)
				.sort((a, b) => b.show.rating.average - a.show.rating.average);
			//console.log(seriesTemp);
		}
		if (filtered.date) {
			seriesTemp = seriesTemp.sort(
				(a, b) => new Date(b.show.premiered) - new Date(a.show.premiered)
			);
			//console.log(seriesTemp);
		}
		setSeries(seriesTemp);
	}

	useEffect(() => {
		changeSeries();
	}, [filtered]);

	useEffect(() => {
		changeSeries();
	}, [results]);

	return (
		<div className="custom-row" style={{ position: 'relative' }}>
			<ul className="list-group" style={{ marginTop: 20, marginLeft: 20 }}>
				<li
					className="list-group-item d-flex justify-content-between align-items-center"
					style={{ paddingBottom: 0 }}
				>
					<div>
						<h2>Recherche: {query}</h2>
						<p>Nombre de resultat: {series && series.length}</p>
					</div>
				</li>
			</ul>
			<ul className="list-group" style={{ marginTop: 20, marginLeft: 20 }}>
				<li className="list-group-item d-flex justify-content-between align-items-center">
					<h4>Filtres:</h4>
				</li>
				<li className="list-group-item d-flex justify-content-between align-items-center custom-row">
					<div className="form-check disabled" style={{ marginLeft: 10 }}>
						<label className="form-check-label">
							<input
								className="form-check-input"
								type="checkbox"
								value={filtered.note}
								onClick={() => {
									setFiltered({ ...filtered, note: !filtered.note });
								}}
							/>
							Note décroissante
						</label>
					</div>
					<div className="form-check disabled" style={{ marginLeft: 10 }}>
						<label className="form-check-label">
							<input
								className="form-check-input"
								type="checkbox"
								value={filtered.date}
								onClick={() => {
									setFiltered({ ...filtered, date: !filtered.date });
								}}
							/>
							Date de première diffusion
						</label>
					</div>
				</li>
			</ul>
			<div className="custom-row" style={{ marginTop: 10, marginLeft: 10 }}>
				{series &&
					series.map(serie => {
						return (
							<div
								className="card text-white bg-secondary mb-3"
								style={{ maxWidth: '40%' }}
							>
								<SerieThumbnail serie={serie.show} key={serie.show.id} />
							</div>
						);
					})}
			</div>
		</div>
	);
};
export default connect(mapStateToProps)(ResearchResult);
