import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchDetails, fetchEpisodes } from '../actions/details';
import '../css/serie.css';

class SerieDetail extends Component {
	state = {
		spoil: [false, false, false, false, false],
	};

	componentDidMount() {
		this.props.fetchDetails(this.props.match.params.id);
		this.props.fetchEpisodes(this.props.match.params.id);
	}

	render() {
		const { details, episodes } = this.props;
		if (!details || !episodes) {
			return <div className="serieDetail is-loading"></div>;
		}
		const { name, premiered, image, summary, officialSite, rating } = details;
		let lastepisodes = Array.from(episodes).slice(-5);
		return (
			<div className="serieDetail">
				<div className="card text-white bg-dark mb-3">
					<h3 className="card-header">{name}</h3>
					<div className="card-body">
						<h5 className="card-title">{premiered}</h5>
						<h6 className="card-subtitle text-muted">
							{rating?.average
								? `Rating average : ${rating.average}`
								: 'Rating average unavailable'}
						</h6>
						<img
							className="img-serie"
							src={`${image && image.medium}`}
							alt={`img ${name}`}
						/>
						<div
							className="card-text espace-top"
							dangerouslySetInnerHTML={{ __html: summary }}
						/>
					</div>
					<ul className="list-group list-group-flush">
						<h5 className="espace-left espace-top card-title">Last episodes</h5>
						{lastepisodes &&
							lastepisodes.map((value, key) => {
								return (
									<li className="list-group-item">
										Season {value.season} Episode {value.number} : {value.name}
										<h6 className="espace-top card-subtitle text-muted">
											{value.airdate}
										</h6>
										{value.image ? (
											<img
												src={`${value.image.medium}`}
												alt={`${value.name}`}
												className="espace-top"
											/>
										) : (
											''
										)}
										<button
											type="button"
											className={`espace-top btn ${
												this.state.spoil[key]
													? 'btn-outline-success'
													: 'btn-outline-danger'
											} btn-lg btn-block`}
											onClick={() => this.handleSpoilClick(key)}
										>
											{this.state.spoil[key] ? 'NO - SPOIL' : 'SPOIL'}
										</button>
										{this.state.spoil[key] ? (
											<p dangerouslySetInnerHTML={{ __html: value.summary }} />
										) : (
											<p></p>
										)}
									</li>
								);
							})}
					</ul>
					{officialSite ? (
						<div className="card-body">
							<a
								title="Official website"
								href={`${officialSite}`}
								className="card-link"
							>
								Official website
							</a>
						</div>
					) : (
						<div className="card-footer text-muted">
							Official website unavailable
						</div>
					)}
				</div>
			</div>
		);
	}

	handleSpoilClick(key) {
		var newspoil = this.state.spoil;
		newspoil[key] = !this.state.spoil[key];
		this.setState({ spoil: newspoil });
	}
}
export default connect(
	state => ({
		details: state.details,
		episodes: state.episodes,
	}),
	{ fetchDetails, fetchEpisodes }
)(SerieDetail);
