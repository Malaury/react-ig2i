import React from 'react';
import { connect } from 'react-redux';
import { search } from '../actions/search';

const mapStateToProps = ({ search }) => ({
	query: search.query,
});

function SearchInput({ query, dispatch }) {
	return (
		<input
			className="form-control mr-sm-2"
			type="text"
			placeholder="Nom de série"
			value={query}
			onChange={e => dispatch(search(e.target.value))}
		/>
	);
}

export default connect(mapStateToProps)(SearchInput);
