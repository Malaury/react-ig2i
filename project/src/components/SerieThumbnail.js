import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const SerieThumbnail = ({
	serie: { id, name, premiered, rating, image, summary },
	onClick,
}) => (
	<Link to={`/series/${name}-${id}`} style={{ color: '#FFF' }}>
		<div className="card-header">
			<div className="custom-row">
				<div style={{ marginLeft: 10 }}>{premiered}</div>
				<div
					style={{
						right: 0,
						position: 'absolute',
						marginRight: 20,
						color: '#FFD700',
					}}
				>
					{rating.average}
				</div>
			</div>
		</div>
		<div className="card-body">
			<h4 className="card-title">{name}</h4>
			<img
				style={{
					width: 200,
					marginLeft: 'auto',
					marginRight: 'auto',
					display: 'block',
				}}
				src={`${image && image.original}`}
				alt={`img ${name}`}
			/>
			<div
				style={{ marginTop: 20 }}
				className="card-text"
				dangerouslySetInnerHTML={{ __html: summary }}
			/>
		</div>
	</Link>
);
SerieThumbnail.propTypes = {
	serie: PropTypes.shape({
		id: PropTypes.number.isRequired,
		name: PropTypes.string.isRequired,
		rating: PropTypes.object,
		premiered: PropTypes.string,
		image: PropTypes.object,
		summary: PropTypes.string.isRequired,
	}).isRequired,
};
export default SerieThumbnail;
