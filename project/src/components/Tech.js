import React from 'react';
import PropTypes from 'prop-types';

const Tech = ({ title, description }) => {
	return (
		<div className="card border-dark mb-3" style={{ maxWidth: 900 }}>
			<div className="card-body">
				<h4 className="card-title">{title}</h4>
				<p className="card-text">{description}</p>
			</div>
		</div>
	);
};

Tech.propTypes = {
	title: PropTypes.string.isRequired,
	description: PropTypes.string.isRequired,
};

export default Tech;
