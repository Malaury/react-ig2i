import React, { useState, useEffect, Component } from 'react';
import { connect } from 'react-redux';
import { load_images, next_img, prev_img } from '../actions/carrousel';
import '../css/carrousel.css';

const mapStateToProps = ({ carrousel }) => ({
	index: carrousel.index,
	images: carrousel.images,
});

class Carrousel extends Component {
	#timeout;

	componentDidMount() {
		this.props.dispatch(load_images());
	}

	componentDidUpdate(prevProps, prevState, snapshot) {
		this.programNext();
	}

	programNext() {
		this.#timeout = setTimeout(() => {
			if (this.#timeout !== null) {
				this.next();
			}
		}, 3000);
	}

	previous() {
		clearTimeout(this.#timeout);
		this.#timeout = null;
		this.props.dispatch(prev_img());
	}

	next() {
		clearTimeout(this.#timeout);
		this.#timeout = null;
		this.props.dispatch(next_img());
	}

	render() {
		const { index, images } = this.props;

		//TODO: Loader
		if (index === null) return <div />;

		const imgSrc = `/assets/img/carrousel/${images[index]}`;

		return (
			<div
				style={{
					backgroundImage: 'url(' + process.env.PUBLIC_URL + imgSrc + ')',
					width: '100%',
					height: 800,
					position: 'relative',
				}}
			>
				<div className="row">
					<div className="previous arrow" onClick={() => this.previous()}>
						<img
							src={
								process.env.PUBLIC_URL + `/assets/img/carrousel/previous.png`
							}
							alt="previous icon"
						/>
					</div>
					<div className="next arrow" onClick={() => this.next()}>
						<img
							src={process.env.PUBLIC_URL + `/assets/img/carrousel/next.png`}
							alt="next icon"
						/>
					</div>
				</div>
			</div>
		);
	}
}

export default connect(mapStateToProps)(Carrousel);
