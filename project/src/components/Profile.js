import React from 'react';
import '../css/profile.css';

const Profile = ({
	firstname,
	fullname,
	nickname,
	img,
	title,
	favoriteShow,
	evaluationPercentage,
}) => {
	return (
		<div className="card border-dark mb-3" style={{ maxWidth: 300 }}>
			<div className="card-header">
				{firstname} {fullname}
			</div>
			<div className="card-header">Surnom: "{nickname}"</div>
			<div className="card-body">
				<h4 className="card-title">{title}</h4>
				<p className="card-text">Série favorite: {favoriteShow}</p>
				<p className="card-text">
					Pourcentage de la note: {evaluationPercentage}%
				</p>
				<img
					src={process.env.PUBLIC_URL + `/assets/img/${img}`}
					alt="image profile"
					style={{ maxWidth: 250 }}
				/>
			</div>
		</div>
	);
};

export default Profile;
