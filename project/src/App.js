import React from 'react';
import '../node_modules/bootswatch/dist/darkly/bootstrap.min.css';
import { Provider } from 'react-redux';
import store from './store/configureStore';
import Navigator from './containers/Navigator';

function App(props) {
	return (
		<Provider store={store}>
			<Navigator />
		</Provider>
	);
}

export default App;
