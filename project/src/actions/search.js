export const SEARCH = 'SEARCH';

export function search(query) {
	return dispatch =>
		fetch(`http://api.tvmaze.com/search/shows?q=:${query}`)
			.then(response => response.json())
			.then(data => {
				dispatch({
					type: SEARCH,
					query: query,
					results: data,
				});
			});
}
