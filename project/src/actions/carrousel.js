import images from '../data/diapo.json';

export const LOAD = 'LOAD';

export function load_images() {
	return {
		type: LOAD,
		images: images,
	};
}

export const NEXT_IMG = 'NEXT_IMG';

export function next_img() {
	return {
		type: NEXT_IMG,
	};
}

export const PREV_IMG = 'PREV_IMG';

export function prev_img() {
	return {
		type: NEXT_IMG,
	};
}
