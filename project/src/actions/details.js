export const DETAILS = 'DETAILS';

export function fetchDetails(id) {
	return dispatch =>
		fetch(`http://api.tvmaze.com/shows/${id}`)
			.then(response => response.json())
			.then(data => {
				dispatch({
					type: DETAILS,
					details: data,
				});
			});
}

export const EPISODES = 'EPISODES';

export function fetchEpisodes(id) {
	return dispatch =>
		fetch(`http://api.tvmaze.com/shows/${id}/episodes`)
			.then(response => response.json())
			.then(data => {
				dispatch({
					type: EPISODES,
					episodes: data,
				});
			});
}
