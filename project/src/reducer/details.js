import { DETAILS } from '../actions/details';

const defaultState = {
	details: null,
};

export default function (state = defaultState, { type, details }) {
	if (type === DETAILS) {
		return details;
	}
	return state;
}
