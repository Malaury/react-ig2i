import { EPISODES } from '../actions/details';

const defaultState = {
	episodes: null,
};

export default function (state = defaultState, { type, episodes }) {
	if (type === EPISODES) {
		return episodes;
	}
	return state;
}
