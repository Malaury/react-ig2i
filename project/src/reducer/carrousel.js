import { LOAD, NEXT_IMG, PREV_IMG } from '../actions/carrousel';

const defaultState = {
	index: null,
	images: [],
};

export default function (state = defaultState, { type, images }) {
	if (type === LOAD) {
		return {
			index: 0,
			images: images,
		};
	}

	if (type === PREV_IMG) {
		const { index, images } = state;
		return {
			...state,
			index: (index + 1) % images.length,
		};
	}

	if (type === NEXT_IMG) {
		const { index, images } = state;
		return {
			...state,
			index: (images.length + index - 1) % images.length,
		};
	}

	return state;
}
