import { combineReducers } from 'redux';
import search from './search';
import details from './details';
import carrousel from './carrousel';
import episodes from './episodes';

export default combineReducers({
	search,
	details,
	carrousel,
	episodes,
});
