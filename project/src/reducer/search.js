import { SEARCH } from '../actions/search';

const defaultState = {
	query: '',
	results: null,
};

export default function (state = defaultState, { type, query, results }) {
	if (type === SEARCH) {
		return {
			query: query,
			results: query === '' ? defaultState.results : results,
		};
	}

	return state;
}
